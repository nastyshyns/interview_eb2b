﻿<?php

require 'classes/PhoneKeyboardConverter.php';
require 'helper.php';

//instantionalization of the class PhoneKeyboardConverter (creation of the object of class PhoneKeyboardConverter)
$phoneKeyboardConverter = new PhoneKeyboardConverter();
//runing the methods of the class PhoneKeyboardConverter
$numeric = $phoneKeyboardConverter->convertToNumeric('Ela nie ma kota');
$string = $phoneKeyboardConverter->convertToString('5,2,22,555,33,222,9999,66,444,55');

//outputing the results
echo $numeric."<br/>";
echo $string;

