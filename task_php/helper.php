﻿<?
/**

* Function raturning the array of rulles

*@rerurn array

*/
function getRules()
{
	return [
		"pres_2"     => "a",
		"pres_22"    => "b",
		"pres_222"   => "c",
		"pres_3"     => "d",
		"pres_33"    => "e",
		"pres_333"   => "f",
		"pres_4"     => "g",
		"pres_44"    => "h",
		"pres_444"   => "i",
		"pres_5"     => "j",
		"pres_55"    => "k",
		"pres_555"   => "l",
		"pres_6"     => "m",
		"pres_66"    => "n",
		"pres_666"   => "o",
		"pres_7"     => "p",
		"pres_77"    => "q",
		"pres_777"   => "r",
		"pres_7777"  => "s",
		"pres_8"     => "t",
		"pres_88"    => "u",
		"pres_888"   => "v",
		"pres_9"     => "w",
		"pres_99"    => "x",
		"pres_999"   => "y",
		"pres_9999"  => "z",
		"pres_0"     => " "
	];
}