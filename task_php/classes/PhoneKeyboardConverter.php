﻿<?php

class PhoneKeyboardConverter
{
	/**
	
	* The array that corresponds presses of the button to the letter from the alphabet
	
	*/
	public $rules;
	
	/**
	
	* In the constructor of the class, the array relating the presses of the button in the telephone to the letter from the alphabet is stored into the property of the class "Rules" from the helper function in "helper.php"
	
	*/
	
	public function __construct()
	{
		
		$this->rules             = getRules();
	}
	
	/**
	* The function validates the input string that should contain only the letters from the alphabet, and spaces.
	* If the input string is valid, then the function returns 1 otherwise the function returns 0
	
	
	* @param string - input string type: string
	
	
	* @return boolean
	*/
	
	public function validateString($string)
	{
		return preg_match("/^[a-z ]*$/",$string);
	}
	
	
	/**
	* Function validates the inpustring that should contain only the numbers from 2 t0 9, and 0, and ",".
	* If the input string valid the function returns 1 otherwise function returns 0
	
	
	* @param string - input string type: string
	
	
	* @return boolean
	*/
	
	
	public function validateNumberic($numberic)
	{
		return preg_match("/^[2-9\,0]*$/",$numberic);
	}
	
	
	/**
	* Function transformes the input string to the format of "Pressing the buttons in telephone".
	* If the input string is valid, than the function returns the string of "Pressing the buttons in telephone" otherwise false with exception
	
	
	* @param string - input string type: string
	
	
	* @return string|false
	*/
	
	public function convertToNumeric($string)
	{
		$string = strtolower($string);
		$phone_keys="";
		
		if($this->validateString($string)) {
			
			$letters_array = str_split($string);
			$numeric_string ="";
			foreach($letters_array as $letter)
			{
				$sub_numeric_string = $this->generateNumericLettersFromLetter($letter);
				if($sub_numeric_string)
					$phone_keys.= $this->generateNumericLettersFromLetter($letter);
				else
					throw new \Exception("Function generateNumericLettersFromLetter returned false, please check it, the input data, and phone_keyboard.json file");
			}
			
			$phone_keys = trim($phone_keys,",");
			return $phone_keys;
			
		}
		else {
			throw new \Exception("The string can contain only the letters expluding 1, and \",\" . Please input the correct data");
			return false;
		}
	}
	
	
	/**
	* Function transformes the input string of the "pressing the buttons in telephone" to words.
	* If the input string valid the function returns the string of the word otherwise false  with exception
	
	
	* @param string - input string type: string
	
	
	* @return string|boolean
	*/
	
	public function convertToString($numbers)
	{
		if($this->validateNumberic($numbers)){
			
			$letters_array = explode(",",$numbers);
			$string = "";
			foreach($letters_array as $letter)
			{
				$sub_string = $this->generateStringFromNumbericLetter($letter);
				if($sub_string) $string.=$sub_string;
			}
			
			return $string;
			
		}
		else{
			throw new \Exception("The string can contain only the numbers and \",\". Please input the correct data");
			return false;
		}
		
	}
	
	/**
	* Function transformes the input letter to the "Pressing the buttons in telephone" to words.
	* If the transformation was failed, than the function returns false
	
	
	* @param letter - input single letter type: string
	
	
	* @return string|false
	*/
	
	public function generateNumericLettersFromLetter($letter)
	{
		$numeric_letter_encoded = array_search($letter,$this->rules);
		if($numeric_letter_encoded !== false){
			$numeric_letter         = str_replace("pres_","",$numeric_letter_encoded);
			return $numeric_letter.",";
		}
		else{
			return false;
		}
	}
	
	
	/**
	* Function transformes the input string of "Pressing the buttons in telephone" in  single letter from alphabet.
	* If the transformation was failed, than the function returns false
	
	
	* @param letter - input string of "Pressing the buttons in telephone" type: string
	
	
	* @return string|false
	*/
	
	public function generateStringFromNumbericLetter($letter)
	{
		if(isset($this->rules["pres_".$letter]))
			return $this->rules["pres_".$letter];
		else
			return false;
	}
	
	
}